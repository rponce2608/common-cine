package pe.com.cine.entity;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tab_pelicula")
@Cacheable(true)
public class Pelicula {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(unique = true, nullable = false)
	private String idPelicula;
	@ManyToOne
	@JoinColumn(name = "idGenero", referencedColumnName = "idGenero", foreignKey = @ForeignKey(name = "idGenero_FK"))
	private Genero genero;
	@Column(length = 100)
	private String titulo_local;

	public String getIdPelicula() {
		return idPelicula;
	}

	public void setIdPelicula(String idPelicula) {
		this.idPelicula = idPelicula;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public String getTitulo_local() {
		return titulo_local;
	}

	public void setTitulo_local(String titulo_local) {
		this.titulo_local = titulo_local;
	}

	/*
	 * idPelicula varchar2(10) not null, idGenero varchar2(50) not null,
	 * titulo_local varchar2(100) not null, titulo_original varchar2(100) not null,
	 * idioma_original varchar2(100) not null, anio_produccion numeric(10) not null,
	 * web varchar2(100) not null, duracion timestamp not null, fecha_estreno date
	 * not null, sinopsis varchar2(800) not null, trailer varchar2(50) not null,
	 * foto varchar2(100) not null,
	 */

}
